package vn.com.viettel.vtit.base.filter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import vn.com.viettel.vtit.base.entity.BaseEntity;

@Getter
@Setter
@AllArgsConstructor
//@NoArgsConstructor
public class FilterJoiner {
    private Class<? extends BaseEntity> clazz;
    private String fileName;
}
