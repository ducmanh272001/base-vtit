package vn.com.viettel.vtit.base.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Slf4j
public class SecurityUtils {


    private SecurityUtils() {
    }

    public static TokenPayload getTokenPayLoad() {
        HttpServletRequest request = getCurrenRequest();
        if (request == null) {
            return null;
        }
        TokenExtractor tokenExtractor = new TokenExtractorImpl();
        JwtTokenDecoder jwtTokenDecoder = new JwtTokenDecoderImpl(new ObjectMapper());
        String currentToken = tokenExtractor.extractorToken(request);
        if (currentToken == null) {
            return null;
        }
        try {
            return jwtTokenDecoder.getTokenPayload(currentToken);
        } catch (JsonProcessingException ex) {
            log.info("Error mapper read json to class payload");
            return null;
        }
    }

    public static Long getUserId() {
        TokenPayload tokenPayload = getTokenPayLoad();
        if (tokenPayload != null) {
            return tokenPayload.getUserId();
        }
        log.info("Token payload is null");
        return null;
    }

    private static HttpServletRequest getCurrenRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
            return ((ServletRequestAttributes) requestAttributes).getRequest();
        }
        log.info("No called to http servlet request");
        return null;
    }
}
