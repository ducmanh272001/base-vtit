package vn.com.viettel.vtit.base.security;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@Slf4j
public class TokenExtractorImpl implements TokenExtractor{
    @Override
    public String extractorTokenHeader(HttpServletRequest request) {
        Enumeration<String> checkToken = request.getHeaders("Authorization");
        String token;
        if(!checkToken.hasMoreElements()){
            return null;
        }
        token =  checkToken.nextElement();
        if(token.startsWith("Bearer")){
            token = token.substring("Bearer".length()).trim();
        }
        return token;
    }

    @Override
    public String extractorTokenParameter(HttpServletRequest request) {
         return request.getParameter("access_token");
    }

    @Override
    public String extractorToken(HttpServletRequest request) {
         String token = extractorTokenHeader(request);
         if(token == null){
             log.info("Trying find token from parameter because no find token header");
             token = extractorTokenParameter(request);
             if(token == null){
                 log.info("No find token from parameter");
             }
         }
         return token;
    }
}
