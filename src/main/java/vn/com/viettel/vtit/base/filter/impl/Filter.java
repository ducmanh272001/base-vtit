package vn.com.viettel.vtit.base.filter.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.lang.NonNull;
import vn.com.viettel.vtit.base.entity.BaseEntity;
import vn.com.viettel.vtit.base.entity.SoftDeleteEntity_;
import vn.com.viettel.vtit.base.filter.FilterBuilder;
import vn.com.viettel.vtit.base.filter.FilterFlag;
import vn.com.viettel.vtit.base.filter.FilterJoiner;
import vn.com.viettel.vtit.base.filter.FilterOperator;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
public class Filter<T> {

    private TypedQuery<T> query;
    private TypedQuery<Long> countQuery;
    private Pageable pageable;


    public Filter(TypedQuery<T> query, TypedQuery<Long> countQuery, Pageable pageable) {
        this.query = query;
        this.countQuery = countQuery;
        this.pageable = pageable;
    }

    public List<T> getList() {
        return query.getResultList();
    }

    public Page<T> getPage() {
        var countRecord = countQuery.getSingleResult();
        //Limit count record return
        query.setMaxResults(pageable.getPageSize());
        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        return new PageImpl<>(getList(), pageable, countRecord);
    }


    public static FilterBuilder builder() {
        return new Builder();
    }


    public static class Builder implements FilterBuilder {

        private StringBuilder statement;
        private String table;
        private String AND_OR;
        private int countTable;
        private final StringBuilder join;
        private StringBuilder paging;
        private Pageable pageable;
        private final StringBuilder filter;
        private final StringBuilder search;
        private final List<Object> parameters;
        private EntityManager entityManager;
        private final String groupBy;


        public Builder() {
            this.statement = new StringBuilder();
            this.groupBy = "";
            this.table = "e. ";
            countTable = 1;
            this.parameters = new ArrayList<>();
            this.join = new StringBuilder();
            this.filter = new StringBuilder();
            this.search = new StringBuilder();
            this.paging = new StringBuilder();
            AND_OR = FilterOperator.AND.getValue();
        }

        public FilterBuilder filter() {
            statement = filter;
            AND_OR = FilterOperator.AND.getValue();
            return this;
        }

        public FilterBuilder isDeletedAt(boolean isDeleted) {
            filter.append(FilterOperator.AND.getValue())
                    .append(table).append(SoftDeleteEntity_.DELETED_AT)
                    .append(isDeleted ? FilterOperator.IS_NOT_NULL.getValue() : FilterOperator.IS_NULL.getValue())
                    .append(" ");
            return this;
        }

        public FilterBuilder search() {
            statement = search;
            AND_OR = FilterOperator.OR.getValue();
            return this;
        }


        public String escape(@NonNull String value) {
            return value.replace("%", "\\%").replace("_", "\\_");
        }


        @Override
        public FilterBuilder isEqual(String fileName, Object value) {
            if (value == null) {
                return this;
            }
            parameters.add(value);
            statement.append(AND_OR).append(table).append(fileName)
                    .append(FilterOperator.EQUAL).append("?").append(parameters.size());
            return this;
        }

        @Override
        public FilterBuilder isNotEqual(String fileName, Object value) {
            if (value == null) {
                return this;
            }
            parameters.add(value);
            statement.append(AND_OR).append(table).append(fileName)
                    .append(FilterOperator.NOT_EQUAL).append("?").append(parameters.size());
            return this;
        }

        @Override
        public FilterBuilder isNull(String fileName) {
            if (fileName == null) {
                return this;
            }
            statement.append(AND_OR).append(table).append(fileName)
                    .append(FilterOperator.IS_NULL);
            return this;
        }

        @Override
        public FilterBuilder isNotNull(String fileName) {
            if (fileName == null) {
                return null;
            }
            statement.append(AND_OR).append(table).append(fileName)
                    .append(FilterOperator.IS_NOT_NULL);
            return this;
        }

        @Override
        public FilterBuilder isIn(String fileName, Collection<?> values) {
            if (fileName == null) {
                return null;
            }
            parameters.add(values);
            statement.append(AND_OR).append(table).append(fileName).
                    append(FilterOperator.IN).append("? (").append(parameters.size()).append(")");
            return this;
        }

        @Override
        public FilterBuilder isNotIn(String fileName, Collection<?> values) {
            if (fileName == null) {
                return null;
            }
            parameters.add(values);
            statement.append(AND_OR).append(table)
                    .append(fileName).append(FilterOperator.NOT_IN)
                    .append("? (").append(parameters.size()).append(")");
            return this;
        }

        @Override
        public FilterBuilder isContains(String fileName, String value) {
            if (fileName == null) {
                return null;
            }
            parameters.add("%" + escape(value) + "%");
            statement.append(AND_OR).append(table)
                    .append(fileName).append(FilterOperator.LIKE).append(parameters.size());
            return this;
        }

        @Override
        public FilterBuilder isNotContains(String fileName, String value) {
            if (fileName == null) {
                return null;
            }
            parameters.add("%" + escape(value) + "%");
            statement.append(AND_OR).append(table)
                    .append(fileName).append(FilterOperator.NOT_LIKE)
                    .append(parameters.size());
            return this;
        }

        @Override
        public FilterBuilder isNotContains(String fileName, String value, FilterFlag flag) {
            if (fileName == null) {
                return null;
            }
            String valueVerify = verifyFilterFlag(flag, value);
            parameters.add("%" + escape(valueVerify) + "%");
            statement.append(AND_OR).append(table)
                    .append(fileName).append(FilterOperator.NOT_LIKE)
                    .append(parameters.size());
            return this;
        }


        public String verifyFilterFlag(FilterFlag flag, String value) {
            if (FilterFlag.UPPER_CASE.equals(flag)) {
                value = value.toUpperCase();
            } else if (FilterFlag.LOWER_CASE.equals(flag)) {
                value = value.toLowerCase();
            }
            return value;
        }


        @Override
        public FilterBuilder isContains(String fileName, String value, FilterFlag flag) {
            if (fileName == null) {
                return null;
            }
            String valueVerify = verifyFilterFlag(flag, value);
            parameters.add("%" + escape(valueVerify) + "%");
            statement.append(AND_OR).append(table)
                    .append(fileName).append(FilterOperator.LIKE)
                    .append(parameters.size());
            return this;
        }


        public FilterBuilder innerJoin(String originFileName, FilterJoiner tableJoin) {
            var count = countTable++;
            var aliasTable = "e" + count;
            table = aliasTable + ".";
            join.append(FilterOperator.JOIN).append(tableJoin.getClazz().getSimpleName())
                    .append(" on ").append(" e.").append(originFileName).append(" = ")
                    .append(table).append(tableJoin.getFileName());
            return this;
        }

        public FilterBuilder rightJoin(String originFileName, FilterJoiner tableJoin) {
            var count = countTable++;
            var aliasTable = "e" + count;
            table = aliasTable + ".";
            join.append(FilterOperator.RIGHT_JOIN).append(tableJoin.getClazz().getSimpleName())
                    .append(" on ").append(" e.").append(originFileName).append(" = ")
                    .append(table).append(tableJoin.getFileName());
            return this;
        }

        public FilterBuilder thenInnerJoin(String originFileName, FilterJoiner tableJoin) {
            var tableCurrent = "e" + countTable++;
            join.append(FilterOperator.INNER_JOIN).append(tableJoin.getClazz().getSimpleName())
                    .append(" ").append(tableCurrent).append(" on ").append(table).append(originFileName);
            var aliasCurrent = tableCurrent + ".";
            join.append(" = ").append(aliasCurrent).append(tableJoin.getFileName());
            return this;
        }

        public FilterBuilder thenLeftJoin(String originFileName, FilterJoiner tableJoin) {
            var tableCurrent = "e" + countTable++;
            join.append(FilterOperator.LEFT_JOIN).append(tableJoin.getClazz().getSimpleName())
                    .append(" ").append(tableCurrent).append(" on ").append(table).append(originFileName);
            var aliasCurrent = tableCurrent + ".";
            join.append(" = ").append(aliasCurrent).append(tableJoin.getFileName());
            return this;
        }

        public FilterBuilder thenRightJoin(String originFileName, FilterJoiner tableJoin) {
            var tableCurrent = "e" + countTable++;
            join.append(FilterOperator.RIGHT_JOIN).append(tableJoin.getClazz().getSimpleName())
                    .append(" ").append(tableCurrent).append(" on ").append(table).append(originFileName);
            var aliasCurrent = tableCurrent + ".";
            join.append(" = ").append(aliasCurrent).append(tableJoin.getFileName());
            return this;
        }

        public FilterBuilder leftJoin(String originFileName, FilterJoiner tableJoin) {
            var count = countTable++;
            var aliasTable = "e" + count;
            table = aliasTable + ".";
            join.append(FilterOperator.LEFT_JOIN).append(tableJoin.getClazz().getSimpleName())
                    .append(" on ").append(" e.").append(originFileName).append(" = ")
                    .append(table).append(tableJoin.getFileName());
            return this;
        }


        public FilterBuilder pageable(Pageable pageable) {
            this.pageable = pageable;
            // Sorting
            Sort sort = pageable.getSort();
            if (sort.isSorted()) {
                paging.append(" order by ");
                for (Sort.Order order : sort) {
                    paging.append("e.").append(order.getProperty()).append(" ").append(order.getDirection().name()).append(",");
                }
                paging.deleteCharAt(paging.length() - 1); // Remove the trailing comma
            }
            return this;
        }
        public <T extends BaseEntity> Filter<T> build(Class<T> entity) {
            StringBuilder queryDefault = new StringBuilder();
            queryDefault.append("select e from ")
                    .append(table).append(entity.getSimpleName()).append(join);
            StringBuilder formatQuery = formatQuery();
            var query = createQuery(queryDefault.append(formatQuery).append(groupBy).append(paging),entity);
            var countQuery = createCountQuery(entity,join,formatQuery);
            return new Filter<>(query,countQuery,pageable);
        }


        public FilterBuilder withEntityManager(EntityManager entityManager) {
            this.entityManager = entityManager;
            return this;
        }

        private <T> TypedQuery<T> createQuery(StringBuilder query, Class<T> clazz) {
            log.info("Create query {}", query.toString());
            var queryBuilder = entityManager.createQuery(query.toString(), clazz);
            for (int i = 1; i < parameters.size(); i++) {
                queryBuilder.setParameter(i, parameters.get(i));
            }
            return queryBuilder;
        }

        private TypedQuery<Long> createCountQuery(Class<? extends BaseEntity> clazz, StringBuilder joinQuery, StringBuilder buildQuery) {
            StringBuilder builder = new StringBuilder();
            builder.append("select count(e) from ").append(clazz.getSimpleName())
                    .append(" e").append(joinQuery);
            builder.append(buildQuery);
            return createQuery(builder,Long.class);
        }

        public StringBuilder formatQuery() {
            StringBuilder builder = new StringBuilder();
            if (search.length() == 0 && filter.length() == 0) {
                return builder;
            }
            builder.append(" where ");
            if (search.length() != 0) {
                builder.append("(").append(search.delete(0, 4)).append(")");
            }
            if (filter.length() != 0) {
                if (search.length() == 0) {
                    filter.delete(0, 4);
                }
                builder.append(filter);
            }
            return builder;
        }


    }


}
