package vn.com.viettel.vtit.base.filter;

import java.util.Collection;

public interface FilterBuilder {
    FilterBuilder isEqual(String fileName, Object value);

    FilterBuilder isNotEqual(String fileName, Object value);

    FilterBuilder isNull(String fileName);

    FilterBuilder isNotNull(String fileName);

    FilterBuilder isIn(String fileName, Collection<?> values);

    FilterBuilder isNotIn(String fileName, Collection<?> values);

    FilterBuilder isContains(String fileName, String value);

    FilterBuilder isNotContains(String fileName, String value);

    FilterBuilder isNotContains(String fileName, String value, FilterFlag flag);

    FilterBuilder isContains(String fileName, String value, FilterFlag flag);
}
