package vn.com.viettel.vtit.base.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Getter;
import lombok.Setter;
import vn.com.viettel.vtit.base.security.SecurityUtils;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.io.Serializable;
import java.time.OffsetDateTime;

@MappedSuperclass
@Getter
@Setter
public abstract class BaseEntity implements EntityID<Long>, Serializable {
    protected OffsetDateTime createdAt;
    protected OffsetDateTime updatedAt;
    protected Long createdBy;
    protected Long updatedBy;

    @PrePersist
    protected void beginCreated() {
        var now = OffsetDateTime.now();
        setCreatedBy(SecurityUtils.getUserId());
        setCreatedAt(now);
    }

    @PreUpdate
    protected void beginUpdated() {
        var now = OffsetDateTime.now();
        setUpdatedBy(SecurityUtils.getUserId());
        setUpdatedAt(now);
    }
}
