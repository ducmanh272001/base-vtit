package vn.com.viettel.vtit.base.repository;

import feign.Param;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;
import vn.com.viettel.vtit.base.entity.SoftDeleteEntity;

@NoRepositoryBean
public interface SoftDeleteJpaRepository<T extends SoftDeleteEntity> extends BaseJpaRepository<T> {
    @Modifying
    @Transactional
    @Query("update #{#entityName} e set e.deletedAt = current_timestamp where e.id = :userId")
    void softDelete(@Param("userId") Long userId);
}
