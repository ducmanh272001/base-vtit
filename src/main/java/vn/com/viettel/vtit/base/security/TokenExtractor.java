package vn.com.viettel.vtit.base.security;

import javax.servlet.http.HttpServletRequest;

public interface TokenExtractor {
    String extractorTokenHeader(HttpServletRequest request);
    String extractorTokenParameter(HttpServletRequest request);
    String extractorToken(HttpServletRequest request);
}
