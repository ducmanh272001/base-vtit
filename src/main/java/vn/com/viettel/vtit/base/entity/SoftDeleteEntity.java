package vn.com.viettel.vtit.base.entity;


import lombok.Getter;
import lombok.Setter;
import vn.com.viettel.vtit.base.security.SecurityUtils;

import javax.persistence.MappedSuperclass;
import java.time.OffsetDateTime;

@MappedSuperclass
@Getter
@Setter
public abstract class SoftDeleteEntity extends BaseEntity{
    protected OffsetDateTime deletedAt;
    protected Long deletedBy;


    @Override
    protected void beginUpdated() {
        super.beginUpdated();
        if(getDeletedAt() != null){
            setDeletedBy(SecurityUtils.getUserId());
        }
    }
}
