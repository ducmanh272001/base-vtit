package vn.com.viettel.vtit.base.filter;

import lombok.Getter;

@Getter
public enum FilterOperator {
    AND(" and "),
    JOIN(" join "),
    LEFT_JOIN(" left join "),
    RIGHT_JOIN(" right join "),
    IS_NULL(" is null "),
    IS_NOT_NULL(" is not null "),
    OR(" or "),
    INNER_JOIN(" join "),
    LIKE(" like "),
    NOT_LIKE(" not like"),
    IN(" in "),
    NOT_IN(" not in "),
    GREATER_THAN(" > "),
    LESS_THAN(" < "),
    EQUAL(" = "),
    NOT_EQUAL(" != ");

    private final String value;

    FilterOperator(String value) {
        this.value = value;
    }
}
