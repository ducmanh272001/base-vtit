package vn.com.viettel.vtit.base.filter;

public enum FilterFlag {
    UPPER_CASE, LOWER_CASE
}
