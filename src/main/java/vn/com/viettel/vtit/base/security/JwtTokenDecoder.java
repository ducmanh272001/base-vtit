package vn.com.viettel.vtit.base.security;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface JwtTokenDecoder {
    TokenPayload getTokenPayload(String token) throws JsonProcessingException;
    RefreshTokenPayload getRefreshTokenPayload(String token) throws JsonProcessingException;
    String decodeHeader(String token);
    String decodePayload(String token);
    String decodeSignature(String token);
}
