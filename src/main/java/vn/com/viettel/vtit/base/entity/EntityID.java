package vn.com.viettel.vtit.base.entity;

public interface EntityID<T> {
    T getId();
    void setId(T id);
}
