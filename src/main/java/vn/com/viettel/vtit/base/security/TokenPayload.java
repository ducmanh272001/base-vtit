package vn.com.viettel.vtit.base.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TokenPayload implements Serializable {
    @JsonProperty("user_name")
    private String username;
    @JsonProperty("scope")
    private List<String> scopes;
    @JsonProperty("authorities")
    private List<String> authorities;
    @JsonProperty("id")
    private Long userId;
    private Long exp;
    private Long iat;
    private String jti;
    @JsonProperty("client_id")
    private String clientId;
}
