package vn.com.viettel.vtit.base.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
public class JwtTokenDecoderImpl implements JwtTokenDecoder {
    private final Base64.Decoder decoder;
    private final ObjectMapper objectMapper;

    public JwtTokenDecoderImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        this.decoder = Base64.getUrlDecoder();
    }

    @Override
    public TokenPayload getTokenPayload(String token) throws JsonProcessingException,ArrayIndexOutOfBoundsException {
        String payload = decodePayload(token);
        if (payload == null) {
            return null;
        }
        String translatePayLoad = new String(decoder.decode(payload));
        return objectMapper.readValue(translatePayLoad, TokenPayload.class);
    }

    @Override
    public RefreshTokenPayload getRefreshTokenPayload(String token) throws JsonProcessingException {
        String payload = decodePayload(token);
        if (payload == null) {
            return null;
        }
        String translatePayLoad = new String(decoder.decode(payload));
        return objectMapper.readValue(translatePayLoad, RefreshTokenPayload.class);
    }

    @Override
    public String decodeHeader(@NonNull String token) {
        String[] arrToken = token.split("\\.");
        return arrToken[0];
    }

    @Override
    public String decodePayload(@NonNull String token) {
        String[] arrToken = token.split("\\.");
        return arrToken[1];
    }

    @Override
    public String decodeSignature(@NonNull String token) {
        String[] arrToken = token.split("\\.");
        return arrToken[3];
    }
}
